/**
 * Created by psybo-03 on 24/10/17.
 */
app.controller('ModalInstanceCtrl', function ($uibModalInstance, items,$scope) {
    $scope.items = items;
    console.log(items);
    $scope.ok = function () {
        $uibModalInstance.close($scope.items);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
