<?php
/**
 * 002_initial_schema.php
 * Date: 09/09/19
 * Time: 12:20 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {
        
       /**
         * Table structure for table 'testimonials'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'designation' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('testimonials');

         /**
         * Table structure for table 'albums'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('albums');

        /**
         /**
         * Table structure for table 'galleries'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'album_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('galleries');

         /**
         * Table structure for table 'news'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'title' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('news');
        
        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('testimonials', TRUE);
        $this->dbforge->drop_table('galleries',TRUE);
        $this->dbforge->drop_table('news', TRUE);
    }
}