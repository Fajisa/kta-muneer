<?php
/**
 * Album_model.php
 * Date: 010/09/19
 * Time: 03:31 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Album_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}