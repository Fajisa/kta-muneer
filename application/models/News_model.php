<?php
/**
 * News_model.php
 * Date: 09/09/19
 * Time: 01:33 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class News_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}