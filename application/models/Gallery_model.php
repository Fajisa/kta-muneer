<?php
/**
 * Gallery_model.php
 * Date: 09/09/19
 * Time: 01:31 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Gallery_model extends MY_Model
{

    function __construct()
    {
    	$this->has_one['album'] = array(
            'foreign_model' => 'Album_model',
             'foreign_table' => 'albums', 
             'foreign_key' => 'id', 
             'local_key' => 'album_id'
        );
        parent::__construct();
        $this->timestamps = TRUE;
    }

}