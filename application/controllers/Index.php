<?php
/**
 * Index.php
 * Date: 09/09/19
 * Time: 12:23 PM
 */

class Index extends CI_Controller { 
	 
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Testimonial_model', 'testimonial');
        $this->load->model('Gallery_model', 'gallery');
        $this->load->model('News_model', 'news');
    }

    protected $current = '';

    public function index()
    {
        $data['testimonials'] = $this->testimonial->order_by('id','desc')->limit(1)->get_all();
        $galleries = $this->gallery->order_by('id','desc')->limit(14)->get_all();
        $data['galleries'] = ($galleries ? array_chunk($galleries, 1) : false);
        // var_dump($data['galleries']);exit;
        $data['newss'] = $this->news->order_by('id','desc')->get_all();

        $this->load->view('index',$data);
    }
    public function moments()
    {
        $this->load->view('moments');
    }
    public function usefulllinks()
    {
        $this->load->view('usefulllinks');
    }
    public function rolesandresponsibilities()
    {
        $this->load->view('rolesandresponsibilities');
    }
    public function awards()
    {
        $this->load->view('awards');
    }
    public function single_blog()
    {
        // $data['newss'] = $this->news->where('id',$id)->get();

        $this->load->view('index-single-blog');
    }
}