<?php
/**
 * Migrate.php
 * Date: 05/03/19
 * Time: 11:15 AM
 */

/**
 * Class Migrate
 */

class Migrate extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('migration');
    }

    function index($version = "")
    {
        if ($this->migration->latest() == FALSE) {
            show_error('Error :  ' . $this->migration->error_string());
        } else {
            echo 'Migration run success';
        }
    }

    function rollback($version = "")
    {
        if ($this->migration->version($version) == FALSE) {
            show_error('Error :  ' . $this->migration->error_string());
        } else {
            echo 'Migration run success';
        }
    }
}
