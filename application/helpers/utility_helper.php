<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create asset path
 *change asset path :- asset_path -> config.php
 *@return string 
 eg : http://www.example.com/asset/
 */
if ( ! function_exists('asset'))
{
    function asset()
    {
        $CI =& get_instance();
//        return base_url().$CI->config->item('asset_path');
        return base_url('assets/');
    }
}

/**
 * Get current working directory instead of getcwd()
 * change current working directory :- working_dir -> config.php
 * @return String
 * eg : home/project/codeigniter/uploads/
 */

if (!function_exists('getwdir')) {
    function getwdir()
    {
        return getcwd() .get_instance()->config->item('working_dir');
    }
}


/**
 * create public path
 * change public path :- config.php->public_path
 * @return String
 * eg : http://www.example.com/public/
 */

if (!function_exists('public_url')) {
    function public_url()
    {
        return base_url() . get_instance()->config->item('public_path');
    }
}
