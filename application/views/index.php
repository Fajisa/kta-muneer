<!doctype html>
<html lang="zxx"> 
<head>
    <meta charset="utf-8">
    <title>KTA Muneer's official website</title>
    <meta name="description" content="KTA Muneer's official Personal website"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Loading Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="<?php echo base_url();?>shortcut icon" href="assets/images/favicon.ico"> 
    <!-- Loading Font Icons -->
    <link href="<?php echo base_url();?>assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <!-- Include all css plugins (below), or include individual files as needed -->
    <link href="<?php echo base_url();?>assets/plugins/jpreloader/css/jpreloader.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <!-- Main CSS -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body id="page-top">  
    <!-- BACKGROUND GRID START -->
    <div class="background-grid height-100 position-fixed">
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100 hidden-xs"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
    </div>
    <!-- //BACKGROUND GRID END -->
    
    <!-- HEADER START -->
    <header class="header">
        <div class="logo text-center">
            <div class="display-table height-100 width-100">
                <div class="display-table-cell vertical-align-middle">
                    <img src="<?php echo base_url();?>assets/img/logo.jpg" alt="" style="border: 2px solid #e91e63;border-radius: 100px;">
                </div>
            </div>
        </div>        
        <div class="nav-trigger nav-trigger-open">
            <div class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </header>
    
    <!-- NAVIGATION START -->
    <div id="navigation" class="navbar navbar-display-block" role="navigation">
        <div class="container-fluid display-table height-100 width-100">
            <div class="nav-wraper display-table-cell width-100">
                <div class="nav-trigger nav-trigger-close">
                    <span></span>
                    <span></span>
                </div>
                <ul class="nav navbar-nav text-center text-small text-uppercase">
                    <li class="page-scroll"><a href="#home">Home</a></li>
                    <li class="page-scroll"><a href="#work-experiences">Career</a></li>
                    <li class="page-scroll"><a href="#portfolio">MOMENTS</a></li>
                    <li class="page-scroll"><a href="#testimonials">say's</a></li>
                    <li class="page-scroll"><a href="#blog">media</a></li>
                    <li class="page-scroll"><a href="usefulllinks">In-Social</a></li>
                    <li class="page-scroll"><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- WRAPPER START -->
    <div class="wrapper">
        <section id="home" class="no-padding-tb width-100">            
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="split-image hero-1">                                
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="hero-split-content">
                                <div class="hero-split-intro">
                                    <div class="contact-block home-v2">
                                        <div class="contact-tex">Mail Me:</div><a href="#" class="link-v1">mail@ktamuneer.in</a>
                                    </div>
                                    <h1 class="heading blue-text">Hello, I’m KTA Muneer. Social Worker, Travel and Tourism Consultant, Globetrotter...</h1>
                                    <p class="grey-text">
                                        In early days, KTA Muneer had keen interest to devote his time for social services and eventually is satisﬁed with the innumerable support that he could contribute to the growth of our society. This gave him ample opportunities to interact with people of all levels and encouragement to carry on his endeavors in this ﬁeld. 
                                    </p>
                                    <p class="grey-text">
                                        Over time, he was hugely involved in social services and doing his bit for the community. He was one among the pioneers and pillars for some of the core changes that has come about in a Pravisi’s life in the last decade.  
                                    </p>
                                    <a href="#" class="button link-button w-inline-block">
                                        <div class="line-button-text">GO TO PORTFOLIO</div><div class="color-hover left"></div>
                                    </a>
                                    
                                    <a href="#" class="location-block home-v2 w-clearfix left-10">
                                        <img src="<?php echo base_url();?>assets/images/icons/arrowdown-icon-b.svg" class="location-icon" alt="">
                                        <div class="location-text black-text">scroll down</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <!-- SECTION - WORK EXPERIENCES START -->
        <section id="work-experiences" class="section no-padding-bottom">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title center text-center">
                                <h2 class="has-sideline letter-spacing-1 text-uppercase">Social Experience</h2>
                                <p>Having over 20+ years of experience in travel and tourism division; holding varied responsibilities across this niche.</p>
                            </div>
                            <div id="panel-group-work" class="panel-group panel-resume">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="profile-detail position-relative width-100">
                                            <div class="content-wrapper">
                                                <h2 class="blue-text">Having over 20+ years of experience</h2>
                                                <p>
                                                    Having over 20+ years of experience in travel and tourism division; holding varied responsibilities in Air France, KLM Royal Douch Airlines, Qatar Airways and diffrent IATA Travel Agencies. To achieve and excel the right balance in social transformation and career fineness. Completing a diploma in travel and tourism; Quite inspired by his father Late Abdul Kareem,
                                                </p>
                                                <p>
                                                    K T A Muneer has molded his ideology, perspectives and actions ﬁrmly focused on the values of peace, democracy, integrity and commitment.
                                                </p>
                                                <!-- <img src="<?php echo base_url();?>assets/images/signature.png" width="200" alt=""> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="portfolio-links mt-md-30">
                                            <p>The current major domains of Muneer’s social activity are detailed as:</p>
                                            <ul class="portfolio-links-list list-unstyled no-margin">
                                                <li class="link-v2">President of Overseas Indian Cultural Congress (OICC).</li>
                                                <li class="link-v2">Vice-President of Sahya Tourism & Pravasi Coporative Socity.</li>
                                                <li class="link-v2">Treasurer of India Forum (Under Patronage of Consolate General of India).</li>


                                                
                                                <li class="link-v2">Director of NIMS Speciality Hospital.</li>
                                                <li class="link-v2">Chairman, Sahya Charitable Trust, Wandoor</li>
                                                <li class="link-v2">NORKA Ex-Follow-up Committee Member.</li>
                                                <li class="link-v2">Steering Committee Member, Jeddah Keralites Forum.</li>
                                                <li class="link-v2">Director, Indian Forum for Socio Economic Development.</li>
                                                <li class="link-v2">Formal General Secretary, OISCA Youth Forum Kerala Chapter.</li>
                                            </ul>
                                            <a href="rolesandresponsibilities" class="button link-button w-inline-block mt-24">
                                                <div class="line-button-text">view more Roles and Responsibilities</div><div class="color-hover left"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="<?php echo base_url();?>assets/img/about.jpg" class="mt-md-30 img-responsive center-block" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- SECTION - PROFILE START -->
        <section id="profile" class="section section-flex">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-image">
                                <div class="image-wraper left display-inline-block position-relative">
                                    <img src="<?php echo base_url();?>assets/images/profile.jpg" alt="" class="img-responsive"/>
                                </div>
                                <img src="<?php echo base_url();?>assets/img/about-2.jpg" alt="" class="side-image-float right img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-7 col-md-offset-1">
                            <div class="profile-detail position-relative width-100">
                                <div class="content-wrapper mt-md-30">
                                    <h2 class="blue-text">the best of some Awards and achivements...</h2>
                                    <ul class="award">
                                        <li>Best Social Worker Award Ministry of Human Resources in 1995-1996 </li>
                                        <li>Honored with Malayaali Rakthna Puraskaaram Malayaali Samskaarika, Vedhi, Varkala Jan 2016</li>
                                        <li>Attended State Level National Integration Camp Kannur on 25 & 26 September 1993.</li>
                                        <li>‘Jadha Captain’, District Level Campaign 19-21 November 1994 for MES 30th Anniversary Celebration at Manjeri.</li>
                                        <li>Attended Natural Camp for Conduct Survey for Trebles at Muthanga, Vayand District Kerala Forest Department on 27-30 December 1994</li>
                                        <li>Honored by Citizens of the City for the Achievements in the ﬁeld of social works.</li>
                                        <li>Attended Pravasi Baradhiya Divas conference organized by Ministry of Overseas Indian Aﬀairs (Govt. of India) 7-9 January 2010, 2011, 2012,2013,2014, 2015, 2017 and 2019</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <ul class="award">
                                <li>Participated International Conference on ‘Engaging Youth in Crème Prevention’ at Kaula Lumpur 01-04Aug 2010 organized by International Youth Center in association with of Ministry of Youth and Sports Malaysia.</li>
                                <li>Attended workshop for youth workers on ‘Role of youth in promoting Environment Protection and Ecological Conservation Vishwa Yuva Kendra and OISCA International from 24-26July 1995 at New Delhi.  </li>
                            </ul>
                        </div>
                        <a href="awards" class="button link-button w-inline-block mt-24">
                            <div class="line-button-text">view more Awards and Achivements</div><div class="color-hover left"></div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- SECTION - PROJECT START -->
        <section id="portfolio" class="projectv2 section">
            <div class="section-content">
                <div class="container">
                    <div class="wrpper">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h2 class="has-sideline letter-spacing-1 text-uppercase">memorable moments</h2>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <?php
                    if (isset($galleries) and $galleries) {
                        foreach ($galleries as $main) {
                        ?>
                        <div class="col-md-3">
                            <?php foreach ($main as $gallery) {
                                ?>
                                <div class="projects popup-container background-white">
                                    <div class="inner-content position-relative">
                                        <div class="project-about position-relative">
                                            <div class="thumb">
                                                <img src="<?php echo $gallery->url . $gallery->file_name;?>" alt="<?php echo $gallery->name;?>" class="img-responsive"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                            ?>    
                        </div>
                        <?php 
                        }
                    }
                    ?>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="moments" class="button link-button w-inline-block mt-24">
                                <div class="line-button-text">view more</div><div class="color-hover left"></div>
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <!-- SECTION - testimonials START -->
        <section id="testimonials" class="section">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <h2 class="blue-text">testimonials</h2>
                            <p>
                                K T A Muneer has molded his ideology, perspectives and actions ﬁrmly focused on the values of peace, democracy, integrity and commitment.
                            </p>
                            <a href="#" class="button link-button w-inline-block mt-24">
                                <div class="line-button-text">let’s work together</div><div class="color-hover left"></div>
                            </a>
                        </div>
                        <div class="col-md-8">
                        <?php
                            if (isset($testimonials) and $testimonials) {
                                foreach ($testimonials as $testimonial) {
                                ?>
                                <div class="testimonials-item">
                                    <div class="testi-img">
                                        <img src="<?php echo $testimonial->url . $testimonial->file_name;?>" alt="<?php echo $testimonial->name;?>">
                                    </div>
                                    <div class="testi-content">
                                        <h4 class="text-white"><?php echo $testimonial->name;?></h4>
                                        <p class="testi-review"><?php echo $testimonial->description;?></p>
                                        <div class="white link-v2"><?php echo $testimonial->designation;?></div>
                                    </div>
                                    <i class="quote fa fa-quote-left"></i>
                                </div>
                                 <?php 
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog" class="blog-v1 section section-flex dark">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title center text-center">
                                <h2 class="has-sideline letter-spacing-1 text-uppercase">In Media Postes</h2>
                                <!-- <p class="white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit voluptas atque pariatur labore, accusantium hic neque rerum ipsum illum.</p> -->
                            </div>
                            <div class="services-v2 position-relative text-center width-100">

                            <?php
                            if (isset($newss) and $newss) {
                                foreach ($newss as $news) {
                                ?>
                                <!--.item -->
                                <div class="service-item display-inline-block position-relative">
                                    <img src="<?php echo $news->url . $news->file_name;?>" alt="<?php echo $news->title;?>">
                                    <div class="service-v2-info">
                                        <a href="index-single-blog"><h4 class="service-v2-heading"><?php echo $news->title;?></h4></a>
                                        <p class="service-v2-description"><?php echo $news->description;?></p>
                                        <a href="<?php echo base_url('index-single-blog/' . $news->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $news->title), '-'); ?>" class="link-v2">Read More</a>
                                    </div>
                                </div>
                                <!-- //.item -->
                                <?php
                                }
                            } 
                            ?>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="news" class="button link-button w-inline-block mt-24">
                                <div class="line-button-text">view more</div><div class="color-hover left"></div>
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>        
        
        <!-- SECTION - CONTACT START -->
        <section id="contact" class="section section-flex">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="profile-detail position-relative width-100">
                                <div class="content-wrapper">
                                    <h2>Contact me now!</h2>
                                    
                                    <form class="contact-v1-form">
                                        <input type="text" class="input half-width w-input" maxlength="256" name="Contact-Name" data-name="Contact Name" placeholder="Your name" id="Contact-Name">
                                        <input type="email" class="input half-width w-input" maxlength="256" name="Contact-Email" data-name="Contact Email" placeholder="Email address" id="Contact-Email" required="">
                                        <input type="text" class="input half-width w-input" maxlength="256" name="Contact-Type" data-name="Contact Type" placeholder="Contact Number" id="Contact-Type">
                                        <input type="text" class="input half-width w-input" maxlength="256" name="Contact-Budget" data-name="Contact Budget" placeholder="Subject" id="Contact-Budget">
                                        <textarea id="Contact-Message" name="Contact-Message" data-name="Contact Message" maxlength="5000" placeholder="Description..." class="input text-area margin-right w-input"></textarea>
                                        <input type="submit" data-wait="Please wait..." value="Send Message" class="button contact-button w-button">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- <div class="profile-image mt-md-45">
                                <div class="image-wraper left display-inline-block position-relative">
                                    <img src="assets/img/contact.jpg" alt="" class="img-responsive">
                                </div>
                                <img src="assets/img/contact2.jpg" alt="" class="side-image-float right img-responsive">
                            </div> -->
                            <div id="fb-root"></div>
                            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
                            <div style="border: 4px solid #e91e63;border-radius: 3px;" class="fb-page" data-href="https://www.facebook.com/K-T-A-Muneer-210702416502891/" data-tabs="timeline" data-width="500" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/K-T-A-Muneer-210702416502891/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/K-T-A-Muneer-210702416502891/">K T A Muneer</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- FOOTER START -->
        <footer class="footer position-relative dark">            
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <h6 class="footer-v2-heading">About Me</h6>
                            <p class="white paragraph-small">
                                Hello, I’m KTA Muneer. Social Worker, Travel And Tourism Consultant, Globetrotter and has molded his ideology, perspectives and actions ﬁrmly focused on the values of peace, democracy, integrity and commitment.
                            </p>
                            <!-- <img src="<?php echo base_url();?>assets/images/signature-w.png" alt="" class="footer-signature"> -->
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <h6 class="footer-v2-heading mt-sm-30">navigation</h6>
                            <ul class="footer-link list-unstyled">
                                <li><a href="#home" class="white">Intro</a></li>
                                <li><a href="#work-experiences" class="white">Career</a></li>
                                <li><a href="#portfolio" class="white">Moments</a></li>
                                <li><a href="#testimonials" class="white">Testimonials</a></li>
                                <li><a href="#contact" class="white">Contact</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h6 class="footer-v2-heading mt-md-30">contact me</h6>
                            <div class="list-wrap">
                                <div class="side-feature-list-item">
                                    <img src="<?php echo base_url();?>assets/images/icons/footer-contact-icon-1.svg" width="20" class="check-mark-icon" alt="">
                                    <div class="foot-c-info">Wandoor, Malappuram, Kerala - INDIA</div>
                                </div>
                                <div class="side-feature-list-item">
                                    <img src="<?php echo base_url();?>assets/images/icons/footer-contact-icon-2.svg" width="20" class="check-mark-icon" alt="">
                                    <div class="foot-c-info">mail@ktamuneer.in</div>
                                </div>
                                <div class="side-feature-list-item">
                                    <img src="<?php echo base_url();?>assets/images/icons/footer-contact-icon-3.svg" width="20" class="check-mark-icon" alt="">
                                    <div class="foot-c-info">00966-556602367</div>
                                </div>
                                <div class="side-feature-list-item">
                                    <img src="<?php echo base_url();?>assets/images/icons/footer-contact-icon-3.svg" width="20" class="check-mark-icon" alt="">
                                    <div class="foot-c-info">+91 9447338254</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h6 class="footer-v2-heading mt-md-30">follow us</h6>
                            <p class="white paragraph-small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. In dolorem maiores unde aut, nulla accusantium minus! Natus molestias.</p>
                            <div class="social-icons home-v2-icons">
                                <a href="https://www.facebook.com/abdul.muneer.3572" target="_blank" class="social-link dark-bg w-inline-block">
                                    <i class="fa fa-facebook hero-social-icon"></i>
                                    <div class="color-hover down"></div>
                                </a>
                                <a href="https://www.instagram.com/kta_muneer/" target="_blank" class="social-link dark-bg w-inline-block">
                                    <i class="fa fa-instagram hero-social-icon"></i>
                                    <div class="color-hover down"></div>
                                </a>
                                <a href="https://twitter.com/ktamuneer" target="_blank" class="social-link dark-bg w-inline-block">
                                    <i class="fa fa-twitter hero-social-icon"></i>
                                    <div class="color-hover down"></div>
                                </a>
                                <a href="https://www.youtube.com/user/jaihindtvsaudi" target="_blank" class="social-link dark-bg w-inline-block">
                                    <i class="fa fa-youtube hero-social-icon"></i>
                                    <div class="color-hover down"></div>
                                </a>
                                <a href="https://www.linkedin.com/in/abdul-muneer-karippathodika-43a90187/" target="_blank" class="social-link dark-bg w-inline-block">
                                    <i class="fa fa-linkedin hero-social-icon"></i>
                                    <div class="color-hover down"></div>
                                </a>


                            </div>
                        </div>
                        
                    </div>
                    <!-- //.row -->
                </div>
                <!-- //.container -->
            </div>
        </footer>
        <!-- //FOOTER END -->

    </div>
    <!-- //WRAPPER END -->
    
    
    <!-- Loading jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
    
    <!-- Loading Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Include all js plugins (below) -->
    <script src="<?php echo base_url();?>assets/plugins/jpreloader/js/jpreloader.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/debouncer/debouncer.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- Loading Theme JS -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    
</body>
</html>