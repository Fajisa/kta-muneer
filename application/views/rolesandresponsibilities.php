<!doctype html>
<html lang="zxx"> 
<head>
    <meta charset="utf-8">
    <title>KTA Muneer's official website</title>
    <meta name="description" content="KTA Muneer's official Personal website"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Loading Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="<?php echo base_url();?>shortcut icon" href="assets/images/favicon.ico"> 
    <!-- Loading Font Icons -->
    <link href="<?php echo base_url();?>assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <!-- Include all css plugins (below), or include individual files as needed -->
    <link href="<?php echo base_url();?>assets/plugins/jpreloader/css/jpreloader.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <!-- Main CSS -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body id="page-top">  
    <!-- BACKGROUND GRID START -->
    <div class="background-grid height-100 position-fixed">
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100 hidden-xs"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
    </div>
    <!-- //BACKGROUND GRID END -->
    
    <!-- HEADER START -->
    <header class="header">
        <div class="logo text-center">
            <div class="display-table height-100 width-100">
                <div class="display-table-cell vertical-align-middle">
                    <img src="<?php echo base_url();?>assets/img/logo.jpg" alt="" style="border: 2px solid #e91e63;border-radius: 100px;">
                </div>
            </div>
        </div>        
        <div class="nav-trigger nav-trigger-open">
            <div class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </header>
    
    <!-- NAVIGATION START -->
    <div id="navigation" class="navbar navbar-display-block" role="navigation">
        <div class="container-fluid display-table height-100 width-100">
            <div class="nav-wraper display-table-cell width-100">
                <div class="nav-trigger nav-trigger-close">
                    <span></span>
                    <span></span>
                </div>
                <ul class="nav navbar-nav text-center text-small text-uppercase">
                    <li class="page-scroll"><a href="index#home">Home</a></li>
                    <li class="page-scroll"><a href="index#work-experiences">Career</a></li>
                    <li class="page-scroll"><a href="index#portfolio">MOMENTS</a></li>
                    <li class="page-scroll"><a href="index#testimonials">say's</a></li>
                    <li class="page-scroll"><a href="index#blog">media</a></li>
                    <li class="page-scroll"><a href="usefulllinks">In-Social</a></li>
                    <li class="page-scroll"><a href="index#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- WRAPPER START -->
    <div class="wrapper">
        <section id="home" class="no-padding-tb width-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="blog-img fix">
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blog-split-content">
                            <div class="hero-split-intro">                                
                                <div class="post-panel nav-left">
                                    <a href="index" class="post-back-link w-inline-block">
                                        <div class="link-v1">Back to Home</div>
                                    </a>
                                    <div class="post-date">MAIL ME: MAIL@KTAMUNEER.IN</div>
                                </div>
                                <h1 class="heading blue-text">Social Experience</h1>
                                <p class="grey-text">
                                    Having over 20+ years of experience in travel and tourism division; holding varied responsibilities across this niche. To achieve and excel the right balance in social transformation and career fineness. Completing a diploma in travel and tourism; Quite inspired by his father Late Abdul Kareem, KTA Muneer has molded his ideology, perspectives and actions ﬁrmly focused on the values of peace, democracy, integrity and commitment.
                                </p>
                                <p class="grey-text">
                                    The current major domains of KTA Muneer’s social activity are detailed as:
                                </p>
                                <h4 class="red-text">1. Expat Services</h4>
                                <p class="grey-text">
                                    Indians form the largest expatriate community in Saudi Arabia. In Jeddah, gateway city to Saudi Arabia for Hajj and Umra pilgrims, the expat problems are more appalling as compared to the other regions of the Kingdom. KTA Muneer’s Saudi life started in 1997, which was a time when the expat populaces, particularly the Kerala community had badly been in need of an expat helpdesk for the possible solutions the diﬃculties regarding labor law, abuse of law, job cheating or any other issues related with company or sponsor. KTA Muneer was actively involved in all possible welfare activities for the wellbeing of the expat communities and their families. He was elected as NORKA Follow-up Committee Member in 2004.
                                </p>
                                <p class="grey-text">
                                    <b>About NORKA</b><br /> The Non Resident Keralites Aﬀairs abbreviated as NORKA is a department of the Government of Kerala formed on December 6, 1996 to redress the grievances of Non-Resident Keralites (NRKs). It is the ﬁrst of its kind formed in any Indian state. The department was formed in an attempt to strengthen the relationship between the Non Resident Keralites and the Government of Kerala and to institutionalize the administrative framework. NORKA Roots, the ﬁled agency of NORKA which was set up in 2002 acts as a forum for addressing the NRKs problem, safeguarding their rights and rehabilitating the returnees.
                                </p>
                                <h6>Roles and Responsibilities</h6>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Studying the varying Keralite expatriate problems   in every nook and cranny of Saudi Arabia and   redressing them</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Putting in place an ideal form of expatriate   empowerment and welfare</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Introducing NORKA and its supportive services to   the Kerala-based expatriate community in Saudi   Arabia during the tenure of Mr. M. M. Hassan as the   Minister for Information and Parliamentary   Aﬀairs, Government of Kerala from 2001–2004   while held the additional portfolio of the Minister   for Non-Resident Keralites' Aﬀairs in the Kerala   Legislative Assembly</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Traveling extensively in Saudi Arabia, doing   general surveys on expat issues and listing them to seek appropriate solutions</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Forming NORKA Follow Up Committee </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Active engagement in the implementation of   various schemes for the Keralite expats in Saudi   Arabia as the Executive Committee Member of   NORKA </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Adorning the position of NORKA’s Saudi Advisory   Committee Member</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Playing a key role in the proper implementation of several welfare schemes and formulating   policies for the rehabilitation emigrant returnees   during the time of Nitaqat, a program of    Saudization for increasing the employment of   Saudi nationals in the private sector</div>
                                    </div>
                                    <h6>Other Voluntary Expat Services</h6>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Helping the expats to settle their visa issues</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Helping the laid oﬀ employees stranded in the   Kingdom without food and other means of living   by oﬀering them food, arranging the required   documents and providing them air tickets to ﬂy   back to India</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Developing and executing schemes through   NORKA and other voluntary agencies for the   rehabilitation of the repatriates in Kerala by   setting up small scale industries and cooperative   ventures</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Oﬀering training and orientation programs to the   youngsters presently working in various sectors in   the Kingdom on the need and importance of   saving money and making wise and productive   investments for future</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Exhorting the parent communities to give    paramount importance to quality education and   healthy lifestyle habits in their children for their   better future </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Conducting similar programs carrying the sole   objective of giving awareness to the children and   housewives on the toil and sweat in the miserable   life of expat people working in lower positions for   curbing this practice in the next generation and   encouraging the children to attain professional   heights.</div>
                                    </div>
                                </div>
                                <h4 class="red-text">2. Socio-Cultural Activities</h4>
                                <p class="grey-text">
                                    KTA Muneer has been holding pivotal position in many socio-cultural organizations. Details as regards this are briefed under the below given headlines.
                                </p>
                                <h6 class="blue-text">President, OICC</h6>
                                <p class="grey-text">
                                    For the last three years, KTA Muneer is holding the position of the President of Overseas Indian Cultural Congress (OICC), Western Regional Committee, Jeddah, Saudi Arabia which is the biggest regional committee of OICC in the Middle East.
                                </p>
                                <h6>Roles and Responsibilities</h6>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Bringing the various non-resident Indians of the   state under one umbrella</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Working for social and economic welfare of   Keralites abroad </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Maintaining friendly relations with the nationals   of the foreign countries</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Projecting the image of India and Kerala in the   interest of proper understanding and doing all   such acts that are conducive to the welfare of the   Indian nationals abroad </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Helping the Indians in distress </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Organizing cultural activities to entertain them, especially Keralites</div>
                                    </div>
                                </div>
                                <h6>Key activities steered in OICC</h6>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Snehasadanam project executed in Malappuram   district which oﬀered shelters for the homeless</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Sahajeevanam project formed and implemented   for the oversees returnees due to job loss</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Navajeevan aimed at repairing damaged houses   owned by the poor</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Expat Helpline exclusively meant for easing oﬀ the   hardships of expat workers</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">NORKA Helpdesk which dedicated its eﬀorts to   ensure NORKA Identity Card for the expats and   help them being aware of and making the right   utilization of Expat Beneﬁciary Schemes    announced by the Government of Kerala</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Prudent Spending and Safe Earning Scheme   motivating the expat communities to cut down   extravaganza and save money for future</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Passport Adalat which helps thousands of expats   to rectify the errors in their passport and    consequently get over their complications through   Adalats initiated by Indian Consulate in Saudi   Arabia</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Edu Fest for oﬀering career guidance and    orientation to higher secondary students</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Smart Cell which trains the student community in   the wise and sensible usage of modern gadgets</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Munnettam for inspiring the members, supporters   and well-wishers with proper mentoring and   potent guidance</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Anti-smoking Seminar organized on the World   Tobacco Day</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Karunyavarsham, literally meaning ‘year of mercy’, the project announced as part of the Saudi visit of   Sri. Oommen Chandy, the honorable Ex Chief   Minister of Kerala which oﬀered air tickets to the   expats suﬀering to go back to homeland and also   distributed uniforms for a School for Diﬀerently   Abled at Pandikkad in Malappuram district</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Hajj Volunteer Cell inaugurated by Sri. Oommen    Chandy focused on extending voluntary services to   Hajj pilgrims</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">History Congress commemorating the birthdays of   eminent national ﬁgures and the gallant struggles   they faced to win India’s independence</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Setting up special booths for adding the names of   expatriates in the Online Kerala Electoral Registrar   who have not yet gotten their Voters’ ID Card</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc"> If tar Parties conducted at various labor camps in   Saudi Arabia</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Raising the need of direct ﬂight services to    Trivandrum and Kozhikode from Jeddah; </div>
                                    </div>
                                </div>
                                <p class="grey-text">
                                    The Committee feels proud and elated of its committed endeavors that pressured the leadership to hold meetings with the Central Government representatives in this regard and obtain activation for the services to and from Trivandrum
                                </p>
                                <h6 class="blue-text">Vice-Chairman, Hajj Welfare Forum</h6>
                                <p class="grey-text">
                                    K T A Muneer served Hajj Welfare Forum, Jeddah as the Joint Convener from 2003 to 2006. From the very next year onwards, he has been in the position of Vice Chairman.
                                </p>
                                <h6>Roles and Responsibilities</h6>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Extending all sorts of voluntary services and   assistance to Hajj pilgrims</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Leading the volunteers on taking care of the   elderly pilgrims, especially those who lose their   way in Mina while performing the challenging   rituals, oﬀering food and refreshment to the   pilgrims, ensuring that they were dropped at the   doorstep of their camps and so on</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Obtaining the complete support of Indian    Consulate for the smooth and neat conduct of   voluntary activities during the pilgrimage</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Collaborating with other similar voluntary cultural   and social forums to constitute organized and   uniﬁed endeavors along similar lines</div>
                                    </div>
                                </div>
                                <h6 class="blue-text">Treasurer, India Forum</h6>
                                <p class="grey-text">
                                    K T A Muneer serves India Forum Under the patronage of Indian Embassy, Riyadh and Consulate General of India, Jeddah as the Treasurer since 2014.
                                </p>
                                <h6>Roles and Responsibilities</h6>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Working on a series of Indian expat issues and   events </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Conducting special programs for kids, families, women empowerment etc</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Organizing programs that boost the morale of   young generation towards education like ‘Return   from Vacation’ and so on</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Creating Kids Club, conducting inspirational   sessions for children etc.</div>
                                    </div>                                    
                                </div>
                                <h6 class="blue-text">Steering Committee Member, Jeddah Keralites Forum (JKF)</h6>
                                <p class="grey-text">
                                    Jeddah Keralites Forum is an umbrella body or a confederation of all Keralite organizations in Saudi Arabia.
                                </p>
                                <h6>Roles and Responsibilities</h6>
                                <p class="grey-text">
                                    Organizing cultural fests for Kerala Community under the patronage of Indian Consulate every year
                                </p>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Honoring and recognizing Keralite people, organizations and endeavors on their    achievements and remarkable feats</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Showcasing various cultural gatherings to uphold   the diverse but uniﬁed existence of Indian    community irrespective of the diﬀerences in terms   of cast, creed, religion and region.</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Organizing philanthropic programs with a    humanitarian intent to help the poor and needy in   the Keralite community</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Working for the academic and social upbringing of the Keralite expat students in Saudi Arabia</div>
                                    </div>
                                </div>
                                <h6 class="blue-text">Director, IFSED</h6>
                                <p class="grey-text">
                                    K T A Muneer is the Director of Indian Forum for Socio Economic Development (IFSED), Jeddah. 
                                </p>
                                <p class="grey-text">
                                    Indian Forum for Socio Economic Development (IFSED) is an organization formed for the mutual beneﬁt of mankind and to promote peace and harmony among the people, as well as to strive for the development of ﬁnancially weaker sections irrespective of religion, caste, creed and sect, to promote alternative ﬁnance and related subjects, to promote organic cultivating techniques, and to create awareness about environment protection and waste management systems.
                                </p>
                                <h6>Roles and Responsibilities</h6>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Conducting seminars, guidance and training   programs and social network campaigns in Basics   of Alternative Economics, Financial Management, Eﬀective Utilization of resources, Productive   Investments, Participatory Financing/Business, Ethical Business, Art of simple Living, Low Cost   House Construction, Family Farming, Smart   Consumerism, Waste Management.</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Conducting meetings with community/social   leaders on re-framing the current approach on   Waqaf Resource Utilization, Utilization of Charity   Funds, Infrastructural Development to a    Productive and Ethical Investments Model and   raising the subject as today’s priority.</div>
                                    </div>
                                </div>
                                <h6 class="blue-text">General Secretary, Karunya Cancer Sahaya Samiti</h6>
                                <p class="grey-text">
                                    In Karunya Cancer Sahaya Samiti, Jeddah, K T A Muneer is in the position of General Secretary. Quite inspired by the humanitarian outlook of Dr P Abdul Kareem, a veteran medical practitioner in Wandoor region, Karunya Cancer Sahaya Samiti got instituted with a pure philanthropic intent. In the initial phase, the scheme oﬀered travel allowances to the extremely poor patients battling the distress of cancer and their bystanders for their journeys to and from Malabar Regional Cancer Centre. Karunya Cancer Sahaya Samiti later diversiﬁed its service spectrum with a spate of pain and palliative services and cancer treatment assistance.
                                </p>
                                <h6 class="blue-text">Director, NIMS</h6>
                                <p class="grey-text">
                                    KTA Muneer is the Director and Gulf Coordinator of NIMS (Noor Institute of Medical Specialties Private Limited), Wandoor, Malappuram. NIMS School of Nursing and NIMS Hospital have resulted in far-reaching transformations in the ﬁeld of healthcare service and education. 
                                </p>
                                <h6 class="blue-text">Vice President, Sahya Arts & Science College</h6>
                                <p class="grey-text">
                                    KTA Muneer is the Vice President of  Sahya Arts & Science College, a self-ﬁnancing tertiary educational institution established at Wandoor in Malappuram in 2013 under the aegis of Sahya Tourism and Pravasi Co-operative Society (STPC) Ltd and aﬃliated to the University of Calicut with the sole intention to mold the younger generation of Rural areas of Malappuram District into qualiﬁed and responsible citizens with deep values who can contribute to the progress of the country India.
                                </p>
                                <p class="grey-text">
                                    KTA Muneer’s committed eﬀorts energized the team to establish Sahya Arts and Science Colleges in eight Panchayats of Malappuram district, which revolutionized the necessity and signiﬁcance of higher education.
                                </p>
                                <h6 class="blue-text">Executive Vice Chairman, Sahya Builders & Developers LLP, Wandoor, Malappuram district</h6>
                                <h6 class="blue-text">General Secretary, Ashraya Special School Supporting Committee, Jeddah</h6>
                                <p class="grey-text">
                                    Ashraya Special School is the brainchild of Dr P Abdul Kareem. The school oﬀers special education and therapeutic rehabilitation training to the children aﬀected with Autism, Mental Retardation, Cerebral Palsy, Dyslexia etc. The school which got established on a land area of 80 cents with the aid of NRI funds collected mainly from KTA Muneer’s social associations in the Gulf Regions has recently been approved by the Government of Kerala.
                                </p>
                                <h6 class="blue-text">Executive Committee Member, M E S Mampad College Alumni Association, Jeddah</h6>
                                <h6 class="blue-text">Chairman, Sahya Charitable Trust, Wandoor, Malappuram district</h6>
                                <h6 class="blue-text">Member, Ashraya Charitable Trust, Wandoor, Malappuram district</h6>
                                <p class="grey-text">Run by Ashraya special school for mentally challenge students</p>
                                <h6 class="blue-text">Member, NIMS Charitable Trust, Wandoor, Malappuram district</h6>
                                <p class="grey-text">Apart from NIMS School of Nursing and Hospital, the trust runs a special scheme which oﬀers free dialysis to the under privileged renal patients.</p>
                                <h6 class="blue-text">Chief Coordinator, Friends of JANASHREE Mission, Saudi Arabia</h6>
                                <p class="grey-text">
                                    The Janasree Sustainable Development Mission (JSDM) is a major micro-ﬁnance institution in Kerala which takes up self-employment training and agricultural activities in a big way. JSDM's main objectives are to create employment for the unemployed and launch a miscroﬁnance institution. All Janasree units in the State are engaged in job training initiatives and adopt cluster system for production and marketing. Agriculture-based projects aimed at achieving food security in the state share the key objectives. The mission also takes up waste treatment projects and organic farming methods.
                                </p>
                                <h6 class="blue-text">General Secretary, Federation of International Travel Employees (FITE), Jeddah</h6>
                                <h6 class="blue-text">Chief Patron, Wandoor Pravasi Koottayma        Expats Association from Wandoor, Jeddah</h6>
                                <h4 class="red-text">3. Media Endeavors</h4>
                                <p class="grey-text">
                                    KTA Muneer is actively engaged as a media person in various ways. He is the Chief Saudi News Correspondent of Jaihind TV, a leading Malayalam Satellite Television Channel. The ex-positions of K T A Muneer in his social service are explained under the below headlines:  
                                </p>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Student activist in Kerala Student Union (KSU) from 1988</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary of MES Youth Wing, Mampad   College (1989)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Vice President – Akhila Kerala Balajana Sakhyam, Malappuram Union (1990)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, KSU District Committee (1990)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary – MES Youth Wing, Wandoor    Committee (1991)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">President, Akhila Kerala Balajana Sakhyam, Malappuram Union (1991)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, Akhila Kerala Balajana Sakhyam, North   Zone, Kozhikode (1992)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Founder President of FEMS Cultural Center (Registered under Societies Act) Wandoor, Malappuram District (1992)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, District Youth Advisory Committee (Government of Kerala), Malappuram (1993)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Master Trainee, Saskarath Mission, Kerala</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Secretary, M E S Youth Wing Malappuram District   Committee (1994)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, OISCA International Youth   Forum, Malappuram Chapter (1994)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Prerak, Old age Education Program (Rural Development Department) Ambalapadi Vayana   Sala, Wandoor (1995)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">President, OISCA International Youth Forum, Malappuram Chapter (1995)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, M E S Wandoor Unit Committee (1995Roles and Responsibilities</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, M E S Wandoor Unit Committee (1995)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, OISCA International Youth   Forum, Kerala State Chapter (1996)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Secretary, Indian Cultural Congress Western   Regional Committee, Jeddah (2000)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, Follow-up Committee for the Action Plan   for Welfare and Venture of NRKs (GO (Rt) No.15/3/NORKA dated 26-02-2003)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, Norka-Roots Advisory Committee in   Kingdom of Saudi Arabia (Norka Dept. Ref.    987/admn/06/Roots dated 23/03/2006)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Joint Convener, Hajj Welfare Forum, Jeddah (2003-06)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary Indian Cultural Congress, Saudi   Arabian National Committee (2005-09)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, MES Mampad College Alumni   Association, Jeddah (2000-11)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Ex Member, NORKA – Roots Advisory Committee, Government of Kerala (2013 - 16)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Executive Member, Indian Media Forum </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary Indian Cultural Congress, Saudi   Arabian National Committee (2005-09) </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, MES Mampad College Alumni   Association, Jeddah (2000-11)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Ex Member, NORKA – Roots Advisory Committee, Government of Kerala (2013 - 16)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Executive Member, Indian Media Forum</div>
                                    </div>
                                </div>
                                <h1 class="heading blue-text">Political Interests</h1>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Started as a Student Activist -     Kerala Student Union (KSU) from 1988</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary of MES Youth Wing - Mampad College (1989)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Vice President – Akhila Kerala Balajana Sakhyam - Malappuram Union (1990)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, KSU District Committee - (1990)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary – MES Youth Wing - Wandoor Committee (1991)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">President, Akhila Kerala Balajana Sakhyam - Malappuram Union (1991)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, Akhila Kerala Balajana Sakhyam - North Zone, Kozhikode (1992)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Founder President of FEMS Cultural Center - (Reg. under Societies Act) Wandoor, Malappuram Dt. (1992)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, District Youth Advisory Committee - (Govt. of Kerala), Malappuram (1993)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Master Trainee, Saskarath Mission Kerala</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Secretary, M E S Youth Wing Malappuram District Committee (1994)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, OISCA International Youth Forum Malappuram Chapter (1994)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Prerak, Old age Education Programme (Rural development dept.) Ambalapadi Vayana Sala, Wandoor (1995)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">President, OISCA International Youth Forum Malappuram Chapter (1995)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, M E S Wandoor Unit Committee (1995)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, OISCA International Youth Forum Kerala State Chapter (1996)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Secretary, Indian Cultural Congress Western Regional Committee, Jeddah (2000)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, Follow-up committee for the action plan for welfare and venture of NRK’s - (GO (Rt)No.15/3/NORKA dated 26-02-2003)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Member, Norka-Roots Advisory Committee Kingdom of Saudi Arabia ( Norka Dept. Ref. 987/admn/06/Roots dated 23/03/2006)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Joint Convener, Hajj Welfare Forum Jeddah (2003-2006)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary Indian Cultural Congress Saudi Arabian National Committee (2005-2009)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">General Secretary, MES Mampad College Alumni Association Jeddah (2000-2011)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Ex Member, NORKA – Roots Advisory Committee Govt. of Kerala (2013 - 2016)</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/images/icons/check-icon.svg" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Ex Executive Member Indian Media Forum</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    <!-- Loading jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
    
    <!-- Loading Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Include all js plugins (below) -->
    <script src="<?php echo base_url();?>assets/plugins/jpreloader/js/jpreloader.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/debouncer/debouncer.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- Loading Theme JS -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    
</body>
</html>