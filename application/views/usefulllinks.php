<!doctype html>
<html lang="zxx"> 
<head>
    <meta charset="utf-8">
    <title>KTA Muneer's official website</title>
    <meta name="description" content="KTA Muneer's official Personal website"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Loading Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="<?php echo base_url();?>shortcut icon" href="assets/images/favicon.ico"> 
    <!-- Loading Font Icons -->
    <link href="<?php echo base_url();?>assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <!-- Include all css plugins (below), or include individual files as needed -->
    <link href="<?php echo base_url();?>assets/plugins/jpreloader/css/jpreloader.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <!-- Main CSS -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body id="page-top">  
    <!-- BACKGROUND GRID START -->
    <div class="background-grid height-100 position-fixed">
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100 hidden-xs"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
    </div>
    <!-- //BACKGROUND GRID END -->
    
    <!-- HEADER START -->
    <header class="header">
        <div class="logo text-center">
            <div class="display-table height-100 width-100">
                <div class="display-table-cell vertical-align-middle">
                    <img src="<?php echo base_url();?>assets/img/logo.jpg" alt="" style="border: 2px solid #e91e63;border-radius: 100px;">
                </div>
            </div>
        </div>        
        <div class="nav-trigger nav-trigger-open">
            <div class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </header>
    
    <!-- NAVIGATION START -->
    <div id="navigation" class="navbar navbar-display-block" role="navigation">
        <div class="container-fluid display-table height-100 width-100">
            <div class="nav-wraper display-table-cell width-100">
                <div class="nav-trigger nav-trigger-close">
                    <span></span>
                    <span></span>
                </div>
                <ul class="nav navbar-nav text-center text-small text-uppercase">
                    <li class="page-scroll"><a href="index#home">Home</a></li>
                    <li class="page-scroll"><a href="index#work-experiences">Career</a></li>
                    <li class="page-scroll"><a href="index#portfolio">MOMENTS</a></li>
                    <li class="page-scroll"><a href="index#testimonials">say's</a></li>
                    <li class="page-scroll"><a href="index#blog">media</a></li>
                    <li class="page-scroll"><a href="usefulllinks">In-Social</a></li>
                    <li class="page-scroll"><a href="index#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    <style type="text/css">
        .texte {
            width: 100%;
    font-weight: 800;
    border-bottom: 2px solid #ccc;
    margin-bottom: 35px;
    text-align: right;
        }
    </style>
    
    <!-- WRAPPER START -->
    <div class="wrapper">
        <section id="home" class="no-padding-tb width-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="blog-img fix text-center" style="padding-top: 70px;background-image: url(../images/awards-op.png) !important;">
                            <a class="linker" target="_blank" href="https://cgijeddah.mkcl.org/">
                                <img src="assets/images/jeddha.png">
                            </a>
                            <a class="linker" target="_blank" href="https://norkaroots.org/">
                                <img src="assets/images/norka.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blog-split-content">
                            <div class="hero-split-intro">                                
                                <div class="post-panel nav-left">
                                    <a href="index" class="post-back-link w-inline-block">
                                        <div class="link-v1">Back to Home</div>
                                    </a>
                                    <div class="post-date">MAIL ME: MAIL@KTAMUNEER.IN</div>
                                </div>
                                <h1 class="heading blue-text">Usefull Information</h1>
                                <h6 class="red-text">Pravasi Welfare Schemes and Fund</h6>
                                <p>
                                    The Non-Resident Keralites' Welfare Fund Act, 2008 has developed some welfare schemes for the benefits of Non-Resident Keralites, such as pension scheme, family pension scheme, pension scheme for invalids, scheme for financial assistance to dependents of a deceased member, scheme for financial assistance for medical treatment, accident –cum-death insurance scheme, scheme for assistance for  marriage of daughters of members, scheme for payment of financial assistance for maternity, scheme for payment of educational grant, scheme for payment of loan for housing & self-employment, etc.
                                </p>
                                <a class="red-text texte" target="_blank" href="http://pravasiwelfarefund.org/index.php/the-scheme">View More</a>
                                <h6 class="red-text">Recruitment from India</h6>
                                <p>
                                    <b>The documents/formalities required for attestation of employment documents.</b><br/>
                                     As per existing guidelines on recruitment of workers from India, employer is required to submit following documents, complete and duly filled in  both in Arabic and english to the Consulate General of India, Jeddah for latter’s examination and attestation, well in advance.
                                </p>
                                <ol>
                                    <li>Model Employment Agreement (separate agreement for each category and specimen agreement in case of group visa attested by MOFA and Chamber of Commerce).  </li>
                                    <li>Power of Attorney (attested by MOFA and Chamber of Commerce). </li>
                                    <li>Demand Letter (attested by MOFA and Chamber of Commerce). </li>
                                    <li>Company-Registration certificate (with authenticated english translation).</li>
                                    <li>Employment-Visa issued by the Labour Ministry (with authenticated english translation).</li>
                                    <li>Original authority letter in the name of bearer of these documents (If the documents are submitted by the person other than the employer).</li>
                                    <li>copy of the contract awarded to the sponsor (in case of bulk recruitment).</li>
                                </ol>
                                <a class="red-text texte" target="_blank" href="https://cgijeddah.mkcl.org/Content.aspx?ID=711&PID=681">View More</a>
                                <h6 class="red-text">How to Register a Labour complaint with the Indian Consulate</h6>
                                <p>
                                    For redressing of their employment-related grievances, Indian workers may contact the Consulate on any working day along with relevant information furnished in the following format or approach the concerned labour-court.
                                </p>
                                <a class="red-text texte" target="_blank" href="https://cgijeddah.mkcl.org/Content.aspx?ID=759&PID=681">View More</a>
                                <h6 class="red-text">Information regarding Death registration of an Indian in the Consulate</h6>
                                <p>
                                    <b>How to Register a Death?</b><br/>
                                    Documents required with English/Arabic translation (two copies each) for registration for death cases/issue of ‘No Objection Certificate’ for local burial/transportation of the mortal remains to India as per wishes of the family:
                                </p>
                                <ol>
                                    <li>Copy of death report/death certificate/medical report with English translation.</li> 
                                    <li>Copy of police report with English translation (in case of unnatural death).</li>                                     
                                    <li>Consent letter from family in India for local burial/transportation of mortal remains to India (duly attested by a Notary) with Arabic translation.  The family may be advised to send a copy of this to Consulate General of India’s  Fax No. 00966-12-2610574</li>
                                    <li>Copy of Passport and Iqama of the deceased.</li>                                     
                                    <li>Copy of Identity card of Attorney (copy of passport and Iqama for foreigners/copy of Tabiya Card (Identity card) for Saudi nationals).</li>
                                    <li>Letter from sponsor addressed to the Consulate undertaking to settle financial dues, arrears of pay, service benefits, insurance, death compensation etc of the deceased.</li>
                                    <li>The following number can be contacted with regard to death related matters after office hours and on closed holidays 0556122301.</li>
                                </ol>
                                <a class="red-text texte" target="_blank" href="https://cgijeddah.mkcl.org/Content.aspx?ID=758&PID=681">View More</a>
                                <h6 class="red-text">List of banned Medicines</h6>
                                <p>Follow the link <a target="_blank" href="http://www.cgijeddah.com/banned%20Medicines.pdf">Click Now</a></p>
                                <a class="red-text texte" target="_blank" href="http://www.cgijeddah.com/banned%20Medicines.pdf">View More</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    <!-- Loading jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
    
    <!-- Loading Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Include all js plugins (below) -->
    <script src="<?php echo base_url();?>assets/plugins/jpreloader/js/jpreloader.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/debouncer/debouncer.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- Loading Theme JS -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    
</body>
</html>