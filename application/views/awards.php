<!doctype html>
<html lang="zxx"> 
<head>
    <meta charset="utf-8">
    <title>KTA Muneer's official website</title>
    <meta name="description" content="KTA Muneer's official Personal website"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Loading Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="<?php echo base_url();?>shortcut icon" href="assets/images/favicon.ico"> 
    <!-- Loading Font Icons -->
    <link href="<?php echo base_url();?>assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <!-- Include all css plugins (below), or include individual files as needed -->
    <link href="<?php echo base_url();?>assets/plugins/jpreloader/css/jpreloader.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <!-- Main CSS -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body id="page-top">  
    <!-- BACKGROUND GRID START -->
    <div class="background-grid height-100 position-fixed">
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100"></span>
        <span class="column height-100 hidden-xs"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
        <span class="column height-100 hidden-xs hidden-sm"></span>
    </div>
    <!-- //BACKGROUND GRID END -->
    
    <!-- HEADER START -->
    <header class="header">
        <div class="logo text-center">
            <div class="display-table height-100 width-100">
                <div class="display-table-cell vertical-align-middle">
                    <img src="<?php echo base_url();?>assets/img/logo.jpg" alt="" style="border: 2px solid #e91e63;border-radius: 100px;">
                </div>
            </div>
        </div>        
        <div class="nav-trigger nav-trigger-open">
            <div class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </header>
    
    <!-- NAVIGATION START -->
    <div id="navigation" class="navbar navbar-display-block" role="navigation">
        <div class="container-fluid display-table height-100 width-100">
            <div class="nav-wraper display-table-cell width-100">
                <div class="nav-trigger nav-trigger-close">
                    <span></span>
                    <span></span>
                </div>
                <ul class="nav navbar-nav text-center text-small text-uppercase">
                    <li class="page-scroll"><a href="index#home">Home</a></li>
                    <li class="page-scroll"><a href="index#work-experiences">Career</a></li>
                    <li class="page-scroll"><a href="index#portfolio">MOMENTS</a></li>
                    <li class="page-scroll"><a href="index#testimonials">say's</a></li>
                    <li class="page-scroll"><a href="index#blog">news</a></li>
                    <li class="page-scroll"><a href="index#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- WRAPPER START -->
    <div class="wrapper">
        <section id="home" class="no-padding-tb width-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="blog-img blog-img2 fix">
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blog-split-content">
                            <div class="hero-split-intro">                                
                                <div class="post-panel nav-left">
                                    <a href="index" class="post-back-link w-inline-block">
                                        <div class="link-v1">Back to Home</div>
                                    </a>
                                    <div class="post-date">MAIL ME: MAIL@KTAMUNEER.IN</div>
                                </div>
                                <h1 class="heading blue-text">Awards</h1>
                                <div class="list-wra" style="margin-bottom:24px">
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Attended State Level National Integration Camp - Kannur on 25 & 26 September 1993.</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">‘Jadha Captain’, District Level Campaign 19-21 November 1994 for MES 30th Anniversary Celebration at Manjeri.</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Attended Natural Camp for Conduct Survey for Trebles at Muthanga, Vayand District - Kerala Forest Department on 27-30 December 1994</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Attended workshop for youth workers on ‘Role of youth in promoting Environment Protection and Ecological Conservation - Vishwa Yuva Kendra and OISCA International from 24-26July 1995 at New Delhi.</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Participated International Conference on ‘Engaging Youth in Crème Prevention’ at Kaula Lumpur - 01-04Aug 2010 organized by International Youth Center in association with of Ministry of Youth and Sports Malaysia.</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Attended Pravasi Baradhiya Divas conference organized by Ministry of Overseas Indian Aﬀairs (Govt. of India) - 7-9 January 2010, 2011, 2012,2013,2014, 2015, 2017 and 2019</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Best Social Worker Award - Ministry of Human Resources in 1995-1996 </div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Honored with Malayaali Rakthna Puraskaaram - Malayaali Samskaarika, Vedhi, Varkala Jan 2016</div>
                                    </div>
                                    <div class="side-feature-list-item">
                                        <img src="assets/img/award.png" width="16" class="check-mark-icon" alt="">
                                        <div class="plan-desc">Honored by Citizens of the City for the Achievements in the ﬁeld of social works</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    <!-- Loading jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
    
    <!-- Loading Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Include all js plugins (below) -->
    <script src="<?php echo base_url();?>assets/plugins/jpreloader/js/jpreloader.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/debouncer/debouncer.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- Loading Theme JS -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    
</body>
</html>